
-- add columnn
ALTER TABLE table_name
  ADD column_name column_definition;

-- delete column
ALTER TABLE table_name
DROP COLUMN field_name;


INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Taylor Swift
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop Rock", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 213, "Country Pop", 3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-10-22", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 456, "Rock, Alternative Rock, Arena Rock", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 204, "Country", 4);

-- Lady Gaga
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-10-05", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 221, "Rock and Roll", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, Rock, Folk Rock", 5);  

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-05-23", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 6);

-- Justin Bieber
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-11-13", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 232, "Dancehall-poptropical Housemoombahton", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-06-15", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 8);

-- Ariana Grande
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-05-20", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "EDM House", 9);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-02-08", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 236, "Pop, R&B", 10);

-- Bruno Mars
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-11-18", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 207, "Funk, Disco, R&B", 11);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-01-21", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 232, "Pop", 12);



-- [SECTION] Advanced Selects
-- To exclude records, use the "!" which is similar to a NOT operator
SELECT * FROM songs WHERE != 11;

-- Greater than and less than symbols
SELECT * FROM songs WHERE id < 11;
SELECT * FROM songs WHERE id > 11;


-- OR Operator
-- Use the OR operator when querying for specific records
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;


-- IN Operator
SELECT * FROM songs WHERE id IN (1, 2, 3);
SELECT * FROM songs WHERE genre IN ("Pop", "K-Pop");


-- Combining Conditions
SELECT * FROM songs WHERE album_id = 4 AND id < 8;

-- Find partial matches
SELECT * FROM songs WHERE song_name LIKE "%a";
SELECT * FROM songs WHERE song_name LIKE "a%";
SELECT * FROM songs WHERE song_name LIKE "%a%";

-- Sorting Records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;


-- [SECTION] Table Joins
SELECT * FROM artists
  JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists
  JOIN albums ON artists.id = albums.artist_id
  JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title FROM artists
  JOIN albums ON artists.id = albums.artist_id
